import React, {useEffect,useState} from "react";
const baseUrl = "http://localhost:5000/api/v1/";
import SessionsTable from "./SessionsTable";

export default function SessionsEndpoint(){
    const [sessions, setSessions] = useState("");
   
    useEffect(()=> {
        fetch(baseUrl + "sessions")
        .then((response) => { 
            return response.json();
        })
        .then((data) => {
            setSessions(data);
        })

        
    }, []);

    return(
        <div>
        {sessions.length > 0 ? (
            <SessionsTable sessions={sessions}/>
        ) : (
            <a></a>
        )}
        </div>
        
    );
}