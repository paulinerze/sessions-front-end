import React from "react";
import PropTypes from "prop-types";

const SessionsTable = ({sessions}) => (
    <table className="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Description</th>
                <th>Length</th>
            </tr>
        </thead>
        <tbody>
            {sessions.map(session => {
                return (
                    <tr key={session.session_id}>
                        <td>{session.session_id}</td>
                        <td>{session.session_name}</td>
                        <td>{session.session_description}</td>
                        <td>{session.session_length}</td>
                    </tr>
                )
            })}
        </tbody>
    </table>
)

SessionsTable.propTypes = {
    sessions: PropTypes.array.isRequired
}

export default SessionsTable;